import express from 'express';


export const logoutRoute = express.Router();

// logout function
logoutRoute.get('/logout', async (req, res) => {
    delete req.session['user'];
    res.redirect('/');
});