import express from 'express';
import expressSession from 'express-session';

export const expressSessionRoute = express.Router();

expressSessionRoute.use(expressSession({
    secret: "Netfix",
    resave: true,
    saveUninitialized: true
}));

expressSessionRoute.use((req, res, next) => {
    if (!req.session['counter']) {
        req.session['counter'] = 0;
    }
    req.session['counter'] += 1;
    next();
});

function pad(num: number) {
    return (num + "").padStart(2, "0");
};


expressSessionRoute.use((req, res, next) => {
    const now = new Date();
    const dateString = now.getFullYear() + "-" + pad(now.getMonth() + 1) + "-"
        + pad(now.getDate()) + " "
        + pad(now.getHours()) + ":"
        + pad(now.getMinutes()) + ":"
        + pad(now.getSeconds());
    console.log(`[${dateString}] Request ${req.path}`);
    next();
});