import { getBugList, renderInfoToEditForm } from './issue1indexGetAll.js';
import { editSingleBugTable } from './issue1editSingleBugTable.js';
window.onload = () => {
  console.log(  localStorage.getItem('editId')  )
  renderInfoToEditForm()
  // getBugList();
  // editSingleBugTable();
};

function setLocalStorage(){
localStorage.getItem('editId');

}

// Set red color to high priority issue
const priorityText = document.querySelectorAll("#priority");
if(priorityText.innerHTML == "High") {
  priorityText.style.color = "#FF0000";
};

// Rich text editor
new FroalaEditor('textarea', {
  placeholderText: 'Type something here...'
});

const logout = document.querySelector('.logout');

logout.onclick = async function(){
    const res = await fetch('/logout'); 
    if(res.status===200 ) {window.location = '/index.html' 
} else {
    window.location = '/404.html' 
}};
