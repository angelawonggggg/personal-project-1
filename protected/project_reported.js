window.onload = function () {
    loadProjectContent();
    
}

async function loadProjectContent() {

    const search = new URLSearchParams(location.search); 
    const id = search.get('id');
    console.log(id);
    localStorage.setItem('project_id', id)
    const res = await fetch(`/project_reported/${id}`);
    const result = await res.json();
    // console.log(projects.rows);
    console.log(result)

    const projectContentBoard = document.querySelector('#projectContentBoard');
    for (let project of result) {
        const hkDate = new Date(project.created_at);
        

        projectContentBoard.innerHTML = `
        <div class="card-body projectContent" data-id='${project.id}'>
            <label>Project name</label>
            <div class='projectTitle input'>${project.name}</div>
            <label>Description</label>
            <div class='description input'>${project.description}</div>
            <label>Created by</label>
            <div class='description input'>${project.user_name}</div>
            <label>Created at</label>
            <div class='description input'>${hkDate}</div>
            <a href="/issue1.html" class='issueListLink'>Click for relevant issues ➟ </a>
        </div>
    `;
    }    
}
//stand-by for use of participant, company functionality ：
// {/* <div class='companyName'>${project.company}</div> */}
// {/* <label>Participants</label> */}
// {/* <div class='participants'>${project.participants}</div> */}
// {/* <label>Description</label> */}


// async function loadIssue() {

    const logout = document.querySelector('.logout');

    logout.onclick = async function(){
        const res = await fetch('/logout'); 
        if(res.status===200 ) {window.location = '/index.html' 
    } else {
        window.location = '/404.html' 
    }};
    