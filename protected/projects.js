

const form = document.querySelector('#projectForm');

form.addEventListener('submit', async function(event) {
    event.preventDefault();

    const form = event.target;
    console.log(1)

    const formObject = {
        projectName: form.projectName.value,
        createdBy: localStorage.getItem('user_id'),
        // company: form.company.value, //停一停 幾複雜 後補
        // participants: form.participants.value, //停一停 幾複雜 後補
        description: form.projectDescription.value
    }; 

    console.log(2)


    const res = await fetch('/projectsFetch', {      
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObject)
    }); 

    console.log(3)


    const result = await res.json();
    console.log(result);
    console.log(res.status);

    
    console.log('status')
    if(res.status === 200) {
        
        form.reset();
        $('.toast').toast('show');

    } else {
        console.log('project submit failed')
    }

});

const logout = document.querySelector('.logout');

logout.onclick = async function(){
    const res = await fetch('/logout'); 
    if(res.status===200 ) {window.location = '/index.html' 
} else {
    window.location = '/404.html' 
}};


const socket = io.connect();
socket.on('new-msg',(data)=>{
document.getElementById("dropdown-menu").innerHTML += `<li><a class="dropdown-item" href="#">${data}</a></li>`;
})

