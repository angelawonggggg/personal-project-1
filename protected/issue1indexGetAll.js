export async function getBugList() {
  // const res = await fetch('/bugTable/g');
  const res = await fetch(`/bugTable/gByproject/${localStorage.getItem('project_id')}`);
  const bugTables = await res.json();
  console.log(bugTables)
  const element = document.createElement('ul');
  element.setAttribute('id', 'wholeBugTable');

 


  //if users ID matches the one who creates it, we assign him/her the Delete & Edit Button
for(let i = 0; i<bugTables.length; i++){
  const hkDate = new Date(bugTables[i].created_at);
  if(localStorage.getItem("user_id") == bugTables[i]["creator_id"]){
    element.innerHTML += `
    <div class='container'>
    <div class='row issueRow'> 
          <div class='usernameDiv'>
            <div>${bugTables[i].user_name}</div>
          </div>
            
          <div class='issueDetailDiv'>   
            <div>${bugTables[i].title}</div>
            <div>${hkDate}</div>
            <div> ${bugTables[i].priority}</div>
            <div> ${bugTables[i].status}</div>
          </div>


          <div class='icons'>
            <a href="issue_form_for_edit.html">
              <i class="fal fa-edit fa-2x icon" bug_id="${bugTables[i].id}" onclick="localStorage.setItem('editId',${bugTables[i].id})"></i>
              </a>
            <i class="far fa-trash-alt fa-2x icon" bug_id="${bugTables[i].id}"></i>
          </div>

        </div>
      </div>       
        `;
  }else{
        element.innerHTML += `
    <div class='container'>
       <div class='row issueRow'> 
          <div class='usernameDiv'>
            <div>${bugTables[i].user_name}</div>
          </div>
            
          <div class='issueDetailDiv'>   
            <div>${bugTables[i].title}</div>
            <div>${hkDate}</div>
            <div> ${bugTables[i].priority}</div>
            <div> ${bugTables[i].status}</div>
          </div>
          <div class='icons'>
            <a href="issue_form_for_edit.html">
              <i class="fal fa-edit fa-2x icon" bug_id="${bugTables[i].id}" onclick="localStorage.setItem('editId',${bugTables[i].id})"></i>
              </a>
          </div>
        </div>
      </div>
          `;

    
  }


    //     `;
    // <div class='container'>
    // <div class='row issueRow'> 
    //     <div class='usernameDiv'>
    //         <div class='issue username'>${bugTable.user_name}</div> 
    //     </div>

    //     <div class='issueDetailDiv'>            
    //         <div class='issue' id='issueTitle'>${bugTable.title}</div>                       
    //         <div class='issue'>${bugTable.created_at}</div>            
    //         <div class='issue'>${bugTable.priority}</div>
    //     </div>

    //     <div class='icons'>
    //         <a href="issue_form_for_edit.html" class='icon'>
    //           <i class="fal fa-edit fa-2x icon" bug_id="${bugTable.id}" onclick="localStorage.setItem('editId',${bugTable.id})"></i>
    //           </a>
    //         <i class="far fa-trash-alt fa-2x icon" bug_id="${bugTable.id}"></i>
    //     </div>  
    // </div>
    // </div>
    //     `;   
    
    // <div class='container'>
    // <div class='row issueRow'> 
    // <div class='usernameDiv'>
    //         <div> user_name: ${bugTables[i].user_name}</div>
    //         </div>
    //         <div> creator_id: ${bugTables[i].creator_id}</div>
    //         <div> created_at: ${bugTables[i].created_at}</div>
    //         <div> <strong>title: ${bugTables[i].title} </strong></div>
    //         <div> description: ${bugTables[i].description}</div>
    //         <div> request_type: ${bugTables[i].request_type}</div>
    //         <div> priority: ${bugTables[i].priority}</div>
    //         <div> browser: ${bugTables[i].browser}</div>
    //         <div> status: ${bugTables[i].status}</div>
    //         <div> assignee_id: ${bugTables[i].assignee_id}</div>
    //         <div> project_id: ${bugTables[i].project_id}</div>
    //         <a href="issue_form_for_edit.html">
    //     </li><br>`;

}


// for (let bugTable of bugTables) {
//     element.innerHTML += `
//         <li>
//             <div> user_name: ${bugTable.user_name}</div>
//             <div> creator_id: ${bugTable.creator_id}</div>
//             <div> created_at: ${bugTable.created_at}</div>
//             <div> <strong>title: ${bugTable.title} </strong></div>
//             <div> description: ${bugTable.description}</div>
//             <div> request_type: ${bugTable.request_type}</div>
//             <div> priority: ${bugTable.priority}</div>
//             <div> browser: ${bugTable.browser}</div>
//             <div> status: ${bugTable.status}</div>
//             <div> assignee_id: ${bugTable.assignee_id}</div>
//             <div> project_id: ${bugTable.project_id}</div>
//             <a href="issue_form_for_edit.html">
//               <i class="fal fa-edit fa-2x" bug_id="${bugTable.id}" onclick="localStorage.setItem('editId',${bugTable.id})"></i>
//               </a>
//             <i class="far fa-trash-alt fa-2x" bug_id="${bugTable.id}"></i>
//         </li><br>`;
// }

  element.addEventListener('click', async (event) => {
    if (!event.target) return;

    if (event.target.matches('.fa-edit')) {
      const bugId = parseInt(event.target.getAttribute('bug_id'));
      const bugTable = bugTables.find((bugTable) => bugTable.id == bugId);
      editSingleBugTable(bugTable);
    }
    else if (event.target.matches('.fa-trash-alt')) {
      const bugId = parseInt(event.target.getAttribute('bug_id'));
      const bugTable = bugTables.find((bugTable) => bugTable.id == bugId);
      const res = await fetch(`/bugTable/d/${bugId}`, { method: 'DELETE' });
      console.log(`Frontend deleting ${bugId}`)
      const result = await res.json();
      getBugList();
    }
  });

  const bugtableRender = document.getElementById('bugtable');
  bugtableRender.innerHTML = '';
  bugtableRender.appendChild(element);
}

//**========== */

export async function renderInfoToEditForm() {
  const res = await fetch(`/bugTable/g/${localStorage.getItem("editId")}`);
  const editTable = await res.json();
  console.log(editTable)
  const element = document.createElement('ul');
  element.setAttribute('id', 'editTable');
  element.innerHTML += `
        <div>
        <form id='issue-form-editing' class='col-lg-12'>
        <input type="hidden" name="id" class="form-control" value=${editTable[0].id}/>
        <br>

        <div class='editHeading'>
        <H3>Edit Form</H3>
        <p>(Remember to press "submit" button on the bottom before you leave)</p>
        </div>

        <label for='issueTitle'>Title
            <input type='text' name='title' id='issueTitle' size='50' value="${editTable[0].title}">
        </label> <br>
        <div class="form-floating">
            <label for="floatingTextarea2">Description</label>
            <textarea class="form-control" name='description' placeholder="Describe the issue" id="floatingTextarea2"
                style="height: 100px">${editTable[0].description}</textarea>
        </div>
         <br>
        <div class="form-floating">
            <label for="floatingSelect">Request type(Please type: To report an issue / To request a new feature / Data)
                <input type='text' name='request_type' id='issueTitle' size='50' value=${editTable[0].request_type}>
            </label> <br>
        </div>
        <div class="form-floating">
            <br>
            <label for="floatingSelect">Priority (Please type: High / Moderate / Low)
                <input type='text' name='priority' id='issueTitle' size='50' value=${editTable[0].priority}>
            </label> <br>
        </div>
        <div>
            <br>
            <label>Which browser are you using? ((Please type: Chrome / Safari / IE / Firefox))
                <input type='text' name='browser' id='issueTitle' size='50' value=${editTable[0].browser}>
            </label>
            <label>Status
                <input type='text' name='status' id='issueTitle' size='50' value=${editTable[0].status}>
            </label>
        `
        if(localStorage.getItem("user_id") == editTable[0]["creator_id"]){
          element.innerHTML += `
              <button id='editsubmitbutton' type="submit" iclass="btn btn-primary" name="submit" value="submit">Submit</button>
              </form>
              <br> </div>`;
        }

  const editTableRender = document.getElementById('editTable');
  editTableRender.innerHTML = '';
  editTableRender.appendChild(element);
  // const button = document.getElementById('post-btn');

  const inputForm = document.getElementById('issue-form-editing');
  document.getElementById("editsubmitbutton").onclick = async (event) => {
    event.preventDefault();
    const inputFormData = {
      title: inputForm['title'].value,
      description: inputForm['description'].value,
      request_type:inputForm['request_type'].value,
      priority:inputForm['priority'].value,
      browser:inputForm['browser'].value,
      status: inputForm['status'].value,
      creator_id: localStorage.getItem("user_id"),
      project_id: localStorage.getItem("project_id")
    };
    const res = await fetch(`/bugTable/put/${editTable[0].id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(inputFormData),
    });
    const result = await res.json();
    console.log(result)
    // inputForm.reset();
    //getBugList();
  };

  // inputForm.onsubmit = async (event) => {
  //   event.preventDefault();
  //   const formToDatabase = {
  //     id: inputForm['id'].value,
  //     title: inputForm['title'].value,
  //     description: inputForm['description'].value,
  //     request_type: inputForm['request_type'].value,
  //     priority: inputForm['priority'].value,
  //     browser: inputForm['browser'].value,
  //     creator_id: localStorage.getItem("user_id"),
  //     project_id: localStorage.getItem("project_id"),
  //   };
  //   const res = await fetch(`/bugTable/put/${formToDatabase.id}`, {
  //     method: 'PUT',
  //     headers: {
  //       'Content-Type': 'application/json; charset=utf-8',
  //     },
  //     body: JSON.stringify(formToDatabase),
  //   });
  //   const result = await res.json();
  //   // inputForm.reset();
  //   // getBugList();
  // };
}
