window.onload = function () {
    loadProject();

}


document.getElementById("welcomeMessage").innerHTML =
    `
<div id='userMessage'>
<h1 class='welcomeMessage'>Welcome, ${localStorage.getItem('user_name')}.</h1>
<p class='role welcomeMessage'>Your role is <em>${localStorage.getItem('user_role')}</em></p> 
</div>` //Randy's code

async function loadProject() {
    const res = await fetch('/projects');
    const projects = await res.json();
    // console.log(projects);
    const projectBoard = document.querySelector('#projectBoard');
    projectBoard.innerHTML = '';

    // const location = new URL('/project_reported.html?id=${project.id}');
    // const search = new URLSearchParams(location.search);

    // for (let project of projects) {
    //     projectBoard.innerHTML += `
    //     <div class='project' data-id='${project.id}'>
    //     <i class="bi bi-x"></i>
    //     <a href="/project_reported.html?id=${project.id}" class="box">${project.name}</a>
    //   </div>`
    // }
    for (let i = 0; i < projects.length; i++) {
        if (localStorage.getItem("user_id") == projects[i]["created_by_id"]) {
            projectBoard.innerHTML += `
                <div class='project' data-id='${projects[i].id}'>
                    <i class="bi bi-x"></i>
                    <a href="/project_reported.html?id=${projects[i].id}" class="box">${projects[i].name}</a>
                </div>`
        } else {
            projectBoard.innerHTML += `
                <div class='project' data-id='${projects[i].id}'>
                    <a href="/project_reported.html?id=${projects[i].id}" class="box">${projects[i].name}</a>
                </div>`
        }
    }



    const projectDivs = document.querySelectorAll('.project');

    for (let projectDiv of projectDivs) {
        if (projectDiv.querySelector('.bi-x')) {
            projectDiv.querySelector('.bi-x').onclick = async function (event) {

                const deleteLabel = event.target;
                const project = deleteLabel.closest('.project');
                const projectId = project.getAttribute('data-id');
                console.log(`deleting projectId ${projectId}`);

                const res = await fetch(`/projects/${projectId}`, {
                    method: "DELETE",
                });

                const result = await res.json();
                loadProject();
            }
        }

    };
};


const logout = document.querySelector('.logout');

logout.onclick = async function () {
    const res = await fetch('/logout');
    if (res.status === 200) {
        window.location = '/index.html'
    } else {
        window.location = '/404.html'
    }
};


const socket = io.connect();
socket.on('new-msg', (data) => {
    document.getElementById("dropdown-menu").innerHTML += `<li><a class="dropdown-item" href="#">${data}</a></li>`;
})







