import { getBugList, renderInfoToEditForm } from './issue1indexGetAll.js';
import { createSingleBugTable } from './issue1CreateSingleBugTable.js';
import { editSingleBugTable } from './issue1editSingleBugTable.js';

window.onload = () => {
    console.log(  localStorage.getItem('editId')  )
    renderInfoToEditForm()
    getBugList();
    createSingleBugTable();
    editSingleBugTable();
  };

function setLocalStorage(){
  localStorage.getItem('editId');
  
}

const logout = document.querySelector('.logout');

logout.onclick = async function(){
    const res = await fetch('/logout'); 
    if(res.status===200 ) {window.location = '/index.html' 
} else {
    window.location = '/404.html' 
}};

const socket = io.connect();
socket.on('new-msg',(data)=>{
document.getElementById("dropdown-menu").innerHTML += `<li><a class="dropdown-item" href="#">${data}</a></li>`;
})

