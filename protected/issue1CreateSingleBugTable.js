import { getBugList } from './issue1indexGetAll.js';

export async function createSingleBugTable() {
  const inputForm = document.getElementById('issue-form');

  inputForm.onsubmit = async (event) => {
    event.preventDefault();
    const inputFormData = {
      title: inputForm['title'].value,
      description: inputForm['description'].value,
      request_type:inputForm['request_type'].value,
      priority:inputForm['priority'].value,
      browser:inputForm['browser'].value,
      status: inputForm['status'].value,
      creator_id: localStorage.getItem('user_id'),
      project_id: localStorage.getItem('project_id')
    };
    const res = await fetch(`/bugTable/p`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(inputFormData),
    });
    const result = await res.json();
    inputForm.reset();
    getBugList();
  };
}

const logout = document.querySelector('.logout');

logout.onclick = async function(){
    const res = await fetch('/logout'); 
    if(res.status===200 ) {window.location = '/index.html' 
} else {
    window.location = '/404.html' 
}};
