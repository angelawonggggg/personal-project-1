const form = document.querySelector('#commentForm');

form.addEventListener('submit', async function (event) {
    event.preventDefault();

    const form = event.target;
    const formObject = {
        commentContent: form.commentContent.value,
        issued_id: localStorage.getItem('editId'),
        user_id: localStorage.getItem('user_id')
    };


    const res = await fetch('/comments', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObject)
    });
    const result = await res.json();

    // console.log(JSON.stringify(result));
    if (res.status === 200) {
        form.reset();
        // $('.toast').toast('show');
    } else {
        console.log('failed to add comment')
    }
    loadComments();
});

async function loadComments() {
    const res = await fetch(`/comments/${localStorage.getItem("editId")}`); //randy's code
    const comments = await res.json();
    console.log(comments)
    const commentBoard = document.querySelector('#commentBoard');
    commentBoard.innerHTML = '';
    // console.log(comments); //randy's code

    // for (let comment of comments) {
    //     // console.log(comment)

    //     const hkDate = new Date(comment.created_at);
    //     commentBoard.innerHTML += `
    //     <div class='comment-box col-lg-12' data-id='${comment.id}'>
    //         <div class='message-row' >
    //             <div class='comment-user'>${comment.user_name}</div> 
    //             <div class='comment-content'>${comment.content}</div>
    //          </div>
    //         <div class='commentDetails'>
               
    //            <div class='time'>${hkDate}</div>
    //            <i class="bi bi-x" id='delete'></i>
    //         </div>
     
    //     </div>
    //     `
    // };
    

    // for (let comment of comments) {
    //     console.log(comment)
    //     commentBoard.innerHTML += `
    //     <div class='comment-box col-lg-12' data-id='${comment.commentid}'>
    //         <i class="bi bi-x-lg"></i>
    //         <div class='message-row' >
    //             <div class='comment-user' style="color:orange">${comment.user_name} </div> 
    //             <span class='comment-user'>(${comment.role})</span> 
    //             <div class='comment-content'>${comment.content}</div>
    //         </div>
    //         <div class='commentDetails'>   
    //            <div class='time'>${comment.created_at}</div>
    //         </div>
    //     </div>
    //     `
    // };

    for(let i=0; i<comments.length; i++){
        const hkDate = new Date(comments[i].created_at);
        if (localStorage.getItem("user_id") == comments[i]["user_id"]) {
            commentBoard.innerHTML += `
                <div class='comment-box col-lg-12' data-id='${comments[i].commentid}'>
                    <i class="bi bi-x-lg"></i>
                    <div class='message-row' >
                        <div class='comment-user' style="color:orange">${comments[i].user_name} </div> 
                        <span class='comment-user'>(${comments[i].role})</span> 
                        <div class='comment-content'>${comments[i].content}</div>
                    </div>
                    <div class='commentDetails'>   
                       <div class='time'>${hkDate}</div>
                    </div>
                </div>
                `
        }else{
            commentBoard.innerHTML += `
                <div class='comment-box col-lg-12' data-id='${comments[i].commentid}'>
                    <div class='message-row' >
                        <div class='comment-user' style="color:orange">${comments[i].user_name} </div> 
                        <span class='comment-user'>(${comments[i].role})</span> 
                        <div class='comment-content'>${comments[i].content}</div>
                    </div>
                    <div class='commentDetails'>   
                       <div class='time'>${hkDate}</div>
                    </div>
                </div>
                `
        }

    }



    const commentDivs = document.querySelectorAll('.comment-box');

    for (let commentDiv of commentDivs) {
        if (commentDiv.querySelector('.bi-x-lg')) {

        commentDiv.querySelector('.bi-x-lg').onclick = async function (event) {
            console.log("i am click!")
            const deleteLabel = event.target;
            const comment = deleteLabel.closest('.comment-box');
            const commentId = comment.getAttribute('data-id');
            console.log(`try to del ${deleteLabel}`);
            console.log(`comment Id: ${commentId}`);
            const res = await fetch(`/comments/${commentId}`, {
                method: "DELETE",
            });

            

            const result = await res.json();
            loadComments();
        };
    }
        // commentDiv.querySelector('.bi-hand-thumbs-up').onclick = async function(event) {
            
        //     const thumb = event.target;
        //     const comment = thumb.closest('.comment-box'); 
        //     const commentId = comment.getAttribute('data-id'); 
        //     console.log(commentId);
        //     const res = await fetch(`/likes/${commentId}`, {
        //         method: "POST", 
        //     });

        //     await res.json();
            
        // };

  };
};

loadComments();


const socket = io.connect();
socket.on('new-msg',(data)=>{
document.getElementById("dropdown-menu").innerHTML += `<li><a class="dropdown-item" href="#">${data}</a></li>`;
})



