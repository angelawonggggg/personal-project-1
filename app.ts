import express from 'express'
import { Client } from 'pg';
import dotenv from 'dotenv';
import { bugTable } from './bugTableRoutes';
import jsonfile from 'jsonfile';


// import { NextFunction, Request, Response } from 'express';
import expressSession from 'express-session';
// import path from 'path';
import { expressSessionRoute } from './expressSessionRoute';
import { googleRoute } from './googleRoute';
import { registerPageRoute } from './registerPageRoute';
import { loginRoute, isLoggedIn, User } from './loginRoute';
import { logoutRoute } from './logoutRoute';
import { pageOf404Route } from './pageOf404Route';
import { googleLoginRoute } from './googleLoginRoute';
import { Server as SocketIO } from 'socket.io';
import http from 'http';
import multer from 'multer'; 
import path from 'path';


const PORT = 8080;
const app = express();
dotenv.config();

app.use('/', expressSessionRoute)

app.use(expressSession({ //enson's code
    secret: "Netfix",
    resave: true,
    saveUninitialized: true
}));

app.use((req, res, next) => { //enson's code
    if (!req.session['counter']) {
        req.session['counter'] = 0;
    }
    req.session['counter'] += 1;

    console.log(req.session);
    next();
});

function pad(num: number) { //enson's code
    return (num + "").padStart(2, "0");
};


app.use((req, res, next) => { //enson's code
    const now = new Date();
    const dateString = now.getFullYear() + "-" + pad(now.getMonth() + 1) + "-"
        + pad(now.getDate()) + " "
        + pad(now.getHours()) + ":"
        + pad(now.getMinutes()) + ":"
        + pad(now.getSeconds());
    console.log(`[${dateString}] Request ${req.path}`);
    next();
});

export const client = new Client({
    host: 'localhost',
    port: 5432,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
})

client.connect()
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve('./protected/uploads'));
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  });
const upload = multer({storage})



//*Randy
app.use('/bugTable', bugTable);
app.use('/issue1', bugTable);

//*Enson
app.use('/', expressSessionRoute)
app.use('/', googleRoute)
app.use('/', loginRoute)
app.use('/', registerPageRoute)
app.use('/', googleLoginRoute)
app.use('/', logoutRoute)
// socketioRoute.io part
const server = http.createServer(app);
export const io = new SocketIO(server);
// socketioRoute.io part



app.post('/home', async (req, res) => { //enson's code
    const users: User[] = await jsonfile.readFile('./users.json')
    const { username, password } = req.body

    let userFound: User | null = null;
    for (let user of users) {
        if (user.username === username && user.password === password) {
            userFound = user;
            break;
        }
    }
    if (userFound) {
        req.session['user'] = userFound;
        res.status(200).json({ success: true })
    } else {
        res.status(401).json({ success: false, msg: "Username/Password is incorrect!" })
    }
});


//Randy area
// app.route('/protected/issue1')
//     .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
//         res.sendFile(`${__dirname}/protected/issue1.html`);
//     })
// app.route('/protected/bugTable')
//     .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
//         res.sendFile(`${__dirname}/protected/firstPage.html`);
//     })

// app.route('/protected/issuereport')
//     .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
//         res.sendFile(`${__dirname}/protected/issue1.html`);
//     })

// app.route('/protected/edit')
//     .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
//         res.sendFile(`${__dirname}/protected/issue_form_for_edit.html`);
//     })
// app.use('/protected/issue1', bugTable);
// app.route('/protected/issue1')
//     .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
//         res.sendFile(`${__dirname}/protected/issue1.html`);
//     })

//Angela area
// import path from 'path'
// const HOME_HTML = path.resolve('./home.html')
// app.get('/home.html',(req,res)=>{
//     console.log('Home page arrived');
// });


//*Angela

// const UPLOADS_DIR = path.resolve('/protected/uploads')
app.post('/comments', upload.single('image'), isLoggedIn, async function (req, res) {
    const content = req.body;
    console.log(content);

    await client.query(`INSERT INTO comments(content, issue_id, user_id, created_at) 
        VALUES ($1, $2, $3, NOW())`,
        [content.commentContent,
        content.issued_id, //randy edition
        content.user_id]); //randy edition

    res.json({ success: true });
});

//randy edition 
app.get('/comments/:id', isLoggedIn, async function (req, res) {
    try {
        const id = parseInt(req.params.id);
        console.log(`get comments where issue id is ${id}`)
        res.json(
            (await client.query(`
            select 
                comments.id as commentId, 
                comments.content,
                created_at,
                issue_id,
                user_id,
                user_name,
                email_address,
                role
                from comments
            INNER JOIN users on users.id = comments.user_id 
            where comments.issue_id=$1`, [id]))
            .rows,
        )
//*應該係呢個：
//         SELECT * FROM comments
// INNER JOIN users on issue_id = users.id 
//             --where comments.issue_id=xxxx
//;
    } catch (e) {
        console.error(e)
        res.status(500).json({ message: "Internal Server Error" })
    }
});

app.post('/projects', isLoggedIn, async function (req, res) {
    const projectDetail = req.body;
    console.log(projectDetail);
    const projectName = projectDetail.projectName;
    const createdBy = projectDetail.createdBy;
    // const company = projectDetail.company; //停一停 幾複雜 後補
    // const participants = projectDetail.participants; //停一停 幾複雜 後補
    const description = projectDetail.description;

    client.query(`INSERT INTO projects(name, description, created_by_id) VALUES ($1, $2, $3)`,
        [projectName, description, createdBy]);
        io.emit('new-msg', 'New project updated.');
});

app.delete('/comments/:id', 
// isLoggedIn, 
async function (req, res) {
    const id = parseInt(req.params.id);
    console.log(`deleting comment ID: ${id}`)
    if (isNaN(id)) {
        res.status(400).json({ msg: "id is not an integer" });
        return;
    }

    await client.query('DELETE from comments where id = $1', [id]);
    res.json({ success: true });
});



app.post('/projectsFetch', isLoggedIn, async function (req, res) {
    const projectDetail = req.body;
    console.log(projectDetail);

    const projectName = projectDetail.projectName;
    const description = projectDetail.projectDescription;


    await client.query(`INSERT INTO projects(name, description, created_at) VALUES ($1, $2, NOW())`,
        [projectName, description]);

    res.json({ success: true });
});

app.get('/projects', isLoggedIn, async function (req, res) {
    const q = req.query.q;
    console.log(q);

    const result = await client.query(`SELECT * from projects;`);
    let projects = result.rows;
    res.json(projects);
});


app.delete('/projects/:id',
 isLoggedIn, 
 async function (req, res) {
    const id = parseInt(req.params.id);
    console.log(id)
    if (isNaN(id)) {
        res.status(400).json({ msg: "id is not an integer" });
        return;
    }

    await client.query('DELETE from projects where id = $1', [id])

    res.json({ success: true });
});



//Angela not yet done
app.get('/project_reported/:id', isLoggedIn, async (req, res) => {
    // const PROJECT_REPORTED = path.resolve('./public/project_reported.html')
    // res.sendFile(PROJECT_REPORTED);
    const id = req.params.id;

    const project = await client.query(
        'SELECT * FROM projects INNER JOIN users on users.id = projects.created_by_id where projects.id=$1',[id]

    );
    res.json(project.rows);
    console.log(project)
    //'SELECT * from projects where id = $1', [id]
    // res.redirect('/project_reported.html');
})

//*testing
// (await client.query(`
            // SELECT * FROM comments
            // INNER JOIN users on users.id = comments.user_id 
            // where comments.issue_id=$1`, [id]))
//             .rows,
//*testing
//*for angela's reference:
// app.post('/comments', async function (req, res) {
//     const content = req.body;

//     client.query(`INSERT INTO comments(content, created_at) VALUES ($1, NOW())`, [content.commentContent]);



// app.post('/projects', async function (req, res) {
//     const projectDetail = req.body;
//     console.log(projectDetail);
//     const projectName = projectDetail.projectName;
//     const company = projectDetail.company;
//     const participants = projectDetail.participants;
//     const description = projectDetail.projectDescription;

//     client.query(`INSERT INTO projects(name, company, participant, description, created_at) VALUES ($1, $2, $3, $4, NOW())`,
//         [projectName, company, participants, description]);

//     res.json({ success: true });
// })



// app.post('/likes/:commentId', isLoggedIn,async (req, res) => {
//     const commentId = parseInt(req.params.commentId);     // Failed to fetch the correct comment ID 
//     if(isNaN(commentId)) {
//         res.status(400).json({msg:"id is not an integer"});
//         return;
//     }
//     const userId = req.session['user'].id;
//     await client.query(`INSERT INTO likes (comment_id, user_id') values ($1, $2)`, [commentId, userId]); 

//     res.json();
// })


app.use(express.static("public"));
app.use(isLoggedIn, express.static("protected"));


app.use(pageOf404Route, express.static("pageOf404Route"))

///Socket io part (need before server listen, can't change)
io.on('connection', (socket) => {

});


//cheange app.listen to server.listen (#socket.io need to change this)
server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`)
});