# A web application for bug tracking

This web application aims to provide a platform for programmers to track bugs in their projects. 

## Usage 
The application enables users to create a new project and classify issues into the corresponding projects. Moreover, it is possible for whoever created the issue to edit the details of it, other users however do not possess the rights to do so but they can log into their account to leave comments. 

It is useful for programmers to keep track of bugs and share ideas when doing group projects. 

### Main features 
- Authentication 
- Authorization 
- Project creation 
- Report issues
- Edit reported issues
- Add comments to reported issues 
- Delete comments 

## Technologies and language
- JavaScript
- Typescript
- Node.js
- Express
- npm
- PostgreSQL

## Credits 
Enson Yeung, Randy Ho 