import express from 'express';
import path from 'path';

export const pageOf404Route = express.Router();


// "404 page" function
pageOf404Route.use((req, res) => {
    res.sendFile(path.resolve('public/404.html'));
})