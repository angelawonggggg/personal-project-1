import express, { NextFunction } from 'express';
import { client } from './app';
import { checkPassword } from './hash';
import { Request, Response } from 'express';
import path from 'path';


export const loginRoute = express.Router();

// "index page" function
loginRoute.post('/home', async (req, res) => {
    const { username, password } = req.body;
    let users: User[] = (await client.query('SELECT * from users where user_name = $1',
        [username])).rows;
    const user = users[0];
    if (user && await checkPassword(password, user.password)) {
        req.session['user'] = user;
        res.status(200).json({ success: true })
    } else {
        res.status(401).json({ success: false, msg: "Username/Password is incorrect!" })
    }
});

// "User" interface
export interface User {
    username: string;
    password: string
}

// "is Logged in" function
export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.session['user']) {
        next();
    } else {
        res.sendFile(path.resolve('./public/index.html'))
    }
}

// "is Logged in API" function
export function isLoggedInAPI(req: Request, res: Response, next: NextFunction) {
    if (req.session['user']) {
        next();
    } else {
        res.status(401).json({ msg: "UnAuthorized" });
    }
}

// "Business Card page" function
loginRoute.post('/businessCardLogin', async function (req, res) {
    const userData = req.session['user'];
    res.json({userData})
})


// "Welcome User" function (Randy)
loginRoute.get('/welcomeUser', async function(req, res) {
    const user = req.session['user'];
    res.json({user});
})
